# Patrick Healy, 2019
# Using wordlist.txt, populate two separate text files with only B and C words.
# These will be used to 

words = open('wordlist.txt')
b_file = open('b-words.txt', 'w')
c_file = open('c-words.txt', 'w')

for word in words:
    if word[0] is 'b':
        b_file.write(word)
    elif word[0] is 'c':
        c_file.write(word)

b_file.close()
c_file.close()